using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EjercicioBancoy3Clientes
{
    public class Meses
    {
        private int meses;
        public int Meses1
        {
            get
            {
                return meses;
            }
            set
            {
                if (value > 0 && value < 13)
                    Meses1 = value;
            }
        }
    }
}
